package com.mintic.sprintboot.web.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.mintic.sprintboot.web.app.models.entity.Role;

public interface IRoleDao  extends CrudRepository<Role, Long>{

}
